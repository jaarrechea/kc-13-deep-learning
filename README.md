# Práctica Deep Learning

El objetivo final de la práctica es predecir el precio de las viviendas de AirBnB de la ciudad de Madrid a partir de un dataset que se encuentra [aquí](https://public.opendatasoft.com/explore/dataset/airbnb-listings/download/?format=csv&disjunctive.host_verifications=true&disjunctive.amenities=true&disjunctive.features=true&q=Madrid&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B), así como establecer una relación general entre las imágenes de las viviendas y su precio. Sólo se procesarán las viviendas que tengan un precio asignado, así como imágenes que puedan ser examinadas y analizadas.

Se han realizados los siguientes modelos:

- Modelos solo con los datos numéricos y categóricos
  - Clasificación
  - Regresión
- Modelos solo con los datos de imágenes
  - Clasificación
  - Regresión
- Modelos datos numéricos, categóricos e imágenes
  - Clasificación
  - Regresión

En esta práctica se ha utilizado el trabajo realizado en la práctica de Machine Learning en la parte relativa al análisis exploratorio de datos (EDA).

Al final, se explica a las conclusiones a las que se llega, en las que se corrobora lo mencionado en clase, en donde ya se adelantaba que los resultados no iban a ser buenos por no tener un buen sistema de extracción de características de imágenes y porque las imágenes no están estructuradas (no sabemos a qué estancia corresponde cada imagen)


El notebook de la práctica se puede encontrar en este enlace compartido de Google Drive https://drive.google.com/file/d/1qE-qKd96y7vlhRiaiUhu9t0cFOsrb6V-/view?usp=sharing o en este mismo repositorio en el archivo *Práctica_DL_JoséÁngelArrechea.ipynb*.

